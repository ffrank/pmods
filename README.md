# PMODs

Python3 drivers for various PMOD-daughterboards:


* PMOD_"MAX5216" : 16-bit DAC, MAX5216
* PMOD_DA3: 16-Bit DAC, AD5541A 
* PMOD_TMP2: 16-bit temperature sensor, ADT7420
* PMOD_DPOT: 256 position 10kOhm digital potentiometer, AD5160
* PMOD_DA4: 8-channel 12-bit DAC 0-2.48V, AD5628-1
* PMOD_AD2: 3/4-channel 12 bit ADC ext/int-reference, AD7991 
* MAX11300PMB1: 20-port Mixed-Signal I/O 12-bit ADC & 12-bit DAC, MAX11300
 
About the PMOD-connector:
* PMOD itself is a physical connector standard for 6 or 12 pins.
* Following the *Digilent Pmod Interface Specification 1.2.0*, the pinning of several protocols (such as SPI, I2C, and UART) **and** of the electrical power supply is standardized.
* Hence such PMOD-daughterboards are plug'n'play on the Raspberry Pi Hat, and on FPGA eval boards for instance.


